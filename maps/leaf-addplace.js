// Init map
var map = L.map( 'map', {
  center: [-7.24917, 112.75083],
  zoom: 12
});

// Define OSM server
L.tileLayer( 'http://10.151.36.37/osm/{z}/{x}/{y}.png', {
  maxzoom: 18,
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a','b','c']
}).addTo( map );

// Add tourism place
for ( var i=0; i < places.length; ++i )
{
 L.marker( [places[i].lat, places[i].lng] )
  .bindPopup( '<a href="' + places[i].url + '" target="_blank"><b>' + places[i].name + '</b></a><br><br><img src="' + places[i].img + '" height="100">' + '<p class="text-small"><b>Jam Operasional: </b>' + places[i].jam +'<br><b>Tiket: </b>' + places[i].tiket +'</p>' )
  .addTo( map );
}

