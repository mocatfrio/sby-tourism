// Init map
var map = L.map( 'map', {
  center: [-7.24917, 112.75083],
  zoom: 12
});

// Define OSM server
L.tileLayer( 'http://10.151.36.37/osm/{z}/{x}/{y}.png', {
  maxzoom: 18,
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a','b','c']
}).addTo( map );

// Routing
L.Routing.control({
      waypoints: [
          L.latLng(-7.2765, 112.7917),
          L.latLng(-7.2458, 112.7378)
      ],
      routeWhileDragging: true,
//  geocoder: L.Control.geocoder().nominatim()
}).addTo(map);

var geocoder = L.Control.geocoder({
  defaultMarkGeocode: false
    })
    .on('markgeocode', function(e) {
  var bbox = e.geocode.bbox;
  var poly = L.polygon([
       bbox.getSouthEast(),
       bbox.getNorthEast(),
       bbox.getNorthWest(),
       bbox.getSouthWest()
  ]).addTo(map);
  map.fitBounds(poly.getBounds());
    })
    .addTo(map);


function createButton(label, container) {
    var btn = L.DomUtil.create('button', '', container);
    btn.setAttribute('type', 'button');
    btn.innerHTML = label;
    return btn;
}

map.on('click', function(e) {
    var container = L.DomUtil.create('div'),
  startBtn = createButton('Start from this location', container),
  destBtn = createButton('Go to this location', container);

    L.popup()
  .setContent(container)
  .setLatLng(e.latlng)
  .openOn(map);

    L.DomEvent.on(startBtn, 'click', function() {
  control.spliceWaypoints(0, 1, e.latlng);
  map1.closePopup();
    });

    L.DomEvent.on(destBtn, 'click', function() {
  control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);
  map1.closePopup();
    });

});

